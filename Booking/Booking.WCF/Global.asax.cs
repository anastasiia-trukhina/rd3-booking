﻿using System;

namespace Booking.WCF
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            AutoMapperConfig.Initialize();
        }
    }
}