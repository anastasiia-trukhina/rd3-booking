﻿using AutoMapper;
using System;
using System.Collections.Generic;

namespace Booking.WCF
{
    public class BookingService : IBookingService
    {
        protected readonly Booking.BusinessLogic.ResourceManager _resourceManager = new Booking.BusinessLogic.ResourceManager();
        protected readonly Booking.BusinessLogic.EmployeeManager _employeeManager = new Booking.BusinessLogic.EmployeeManager();
        protected readonly Booking.BusinessLogic.MeetingManager _meetingManager = new Booking.BusinessLogic.MeetingManager();

        public IEnumerable<Resource> GetResourcesByType(int typeId)
        {
            return Mapper.Map<IEnumerable<Resource>>(_resourceManager.GetResourcesByType(typeId));
        }

        public Employee SignIn(string login, string password)
        {
            return Mapper.Map<Employee>(_employeeManager.SignIn(login, password));
        }

        public IEnumerable<Resource> GetAllResources()
        {
            return Mapper.Map<IEnumerable<Resource>>(_resourceManager.GetAllResources());
        }

        public IEnumerable<Employee> GetAllEmployees()
        {
            return Mapper.Map<IEnumerable<Employee>>(_employeeManager.GetAllEmployees());
        }

        public void EmployeeRegistation(Employee employee)
        {
            var EmployeeToBL = Mapper.Map<Booking.Repositories.Models.Employee>(employee);
            _employeeManager.EmployeeRegistration(EmployeeToBL);
        }

        public Employee GetEmployeeByLoginAndPassword(string login, string password)
        {
            return Mapper.Map<Employee>(_employeeManager.GetEmployeeByLoginAndPassword(login, password));
        }

        public Employee GetEmployeeById(int id)
        {
            return Mapper.Map<Employee>(_employeeManager.GetEmployeeById(id));
        }

        public Employee GetEmployeeByLogin(string login)
        {
            return Mapper.Map<Employee>(_employeeManager.GetEmployeeByLogin(login));
        }

        public IEnumerable<Role> GetRoles()
        {
            return Mapper.Map<IEnumerable<Role>>(_employeeManager.GetRoles());
        }

        public IEnumerable<Post> GetPosts()
        {
            return Mapper.Map<IEnumerable<Post>>(_employeeManager.GetPosts());
        }

        public Meeting GetMeetingAsParticipant(DateTime date, int userId)
        {
            return Mapper.Map<Meeting>(_meetingManager.GetMeetingAsParticipant(date, userId));
        }

        public Meeting GetMeetingAsOrganizer(DateTime date, int userId)
        {
            return Mapper.Map<Meeting>(_meetingManager.GetMeetingAsOrganizer(date, userId));
        }

        public IEnumerable<Employee> GetAllParticipants(int userId)
        {
            return Mapper.Map<IEnumerable<Employee>>(_employeeManager.GetAllParticipants(userId));
        }

        public IEnumerable<Employee> GetAllOrganizers(int userId)
        {
            return Mapper.Map<IEnumerable<Employee>>(_employeeManager.GetAllOrganizers(userId));
        }

        public void SetMeeting(Meeting meeting)
        {
            var MeetingToBL = Mapper.Map<Booking.Repositories.Models.Meeting>(meeting);
            _meetingManager.SetMeeting(MeetingToBL);
        }

        public IEnumerable<Meeting> GetThreeClosestMeetings(int userId)
        {
            return Mapper.Map<IEnumerable<Meeting>>(_meetingManager.GetThreeClosestMeetings(userId));
        }

        public Meeting GetOrganizersClosestMeeting(int userId)
        {
            return Mapper.Map<Meeting>(_meetingManager.GetOrganizersMeeting(userId));
        }

        public Meeting GetMeetingsAsParticipant(int userId)
        {
            return Mapper.Map<Meeting>(_meetingManager.GetMeetingsAsParticipant(userId));
        }

        public Meeting GetMeetingsAsOrganizer(int userId)
        {
            return Mapper.Map<Meeting>(_meetingManager.GetMeetingsAsOrganizer(userId));
        }

        public void DeleteMeeting(int id)
        {
            _meetingManager.DeleteMeeting(id);
        }

        public void EditMeeting(Meeting meeting)
        {
            var MeetingToBL = Mapper.Map<Booking.Repositories.Models.Meeting>(meeting);
            _meetingManager.EditMeeting(MeetingToBL);
        }

        public IEnumerable<TypeOfResource> GetTypes()
        {
            return Mapper.Map<IEnumerable<TypeOfResource>>(_resourceManager.GetAllTypes());
        }

        public void AddResource(Resource resource)
        {
            var ResourceToBL = Mapper.Map<Booking.Repositories.Models.Resource>(resource);
            _resourceManager.AddResource(ResourceToBL);
        }

        public void AddTypeOfResource(TypeOfResource type)
        {
            var TypeOfResourceToBL = Mapper.Map<Booking.Repositories.Models.TypeOfResource>(type);
            _resourceManager.AddTypeOfResource(TypeOfResourceToBL);
        }

        public void UpdateResource(Resource resource)
        {
            var ResourceToBL = Mapper.Map<Booking.Repositories.Models.Resource>(resource);
            _resourceManager.UpdateResource(ResourceToBL);
        }

        public void DeleteResource(int id)
        {
            _resourceManager.DeleteResource(id);
        }
    }
}
