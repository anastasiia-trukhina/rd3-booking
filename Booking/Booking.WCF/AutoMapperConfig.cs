﻿namespace Booking.WCF
{
    public class AutoMapperConfig
    {
        public static void Initialize()
        {
            AutoMapper.Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Booking.Repositories.Models.Contact, Contact>();
                cfg.CreateMap<Contact, Booking.Repositories.Models.Contact>();
                cfg.CreateMap<Booking.Repositories.Models.Employee, Employee>();
                cfg.CreateMap<Employee, Booking.Repositories.Models.Employee>();
                cfg.CreateMap<Booking.Repositories.Models.Meeting, Meeting>();
                cfg.CreateMap<Meeting, Booking.Repositories.Models.Meeting>();
                cfg.CreateMap<Booking.Repositories.Models.Post, Post>();
                cfg.CreateMap<Post, Booking.Repositories.Models.Post>();
                cfg.CreateMap<Booking.Repositories.Models.Resource, Resource>();
                cfg.CreateMap<Resource, Booking.Repositories.Models.Resource>();
                cfg.CreateMap<Booking.Repositories.Models.Role, Role>();
                cfg.CreateMap<Role, Booking.Repositories.Models.Role>();
                cfg.CreateMap<Booking.Repositories.Models.TypeOfResource, TypeOfResource>();
                cfg.CreateMap<TypeOfResource, Booking.Repositories.Models.TypeOfResource>();
            });
        }
    }
}