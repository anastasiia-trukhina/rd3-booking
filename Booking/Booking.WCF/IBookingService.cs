﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace Booking.WCF
{
    [ServiceContract]
    public interface IBookingService
    {
        [OperationContract]
        Employee SignIn(string login, string password);

        [OperationContract]
        IEnumerable<Employee> GetAllEmployees();

        [OperationContract]
        void EmployeeRegistation(Employee employee);

        [OperationContract]
        Employee GetEmployeeByLoginAndPassword(string login, string password);

        [OperationContract]
        Employee GetEmployeeById(int id);

        [OperationContract]
        Employee GetEmployeeByLogin(string login);

        [OperationContract]
        IEnumerable<Resource> GetAllResources();

        [OperationContract]
        IEnumerable<Resource> GetResourcesByType(int typeId);

        [OperationContract]
        IEnumerable<Role> GetRoles();

        [OperationContract]
        IEnumerable<Post> GetPosts();

        [OperationContract]
        IEnumerable<Employee> GetAllParticipants(int userId);

        [OperationContract]
        IEnumerable<Employee> GetAllOrganizers(int userId);

        [OperationContract]
        Meeting GetMeetingAsParticipant(DateTime date, int userId);

        [OperationContract]
        Meeting GetMeetingAsOrganizer(DateTime date, int userId);

        [OperationContract]
        void SetMeeting(Meeting meeting);

        [OperationContract]
        IEnumerable<Meeting> GetThreeClosestMeetings(int userId);

        [OperationContract]
        Meeting GetOrganizersClosestMeeting(int userId);

        [OperationContract]
        Meeting GetMeetingsAsParticipant(int userId);

        [OperationContract]
        Meeting GetMeetingsAsOrganizer(int userId);

        [OperationContract]
        void DeleteMeeting(int id);

        [OperationContract]
        void EditMeeting(Meeting meeting);

        [OperationContract]
        IEnumerable<TypeOfResource> GetTypes();

        [OperationContract]
        void AddResource(Resource resource);

        [OperationContract]
        void AddTypeOfResource(TypeOfResource type);

        [OperationContract]
        void UpdateResource(Resource resource);

        [OperationContract]
        void DeleteResource(int id);
    }

    [DataContract]
    public class AuthRequest
    {
        [DataMember]
        public string Login { get; set; }

        [DataMember]
        public string Password { get; set; }
    }

    [DataContract]
    public class Resource
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public TypeOfResource TypeOfResource { get; set; }

        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public int Capacity { get; set; }

        [DataMember]
        public ICollection<Meeting> Meetings { get; set; }
    }

    [DataContract]
    public class TypeOfResource
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public ICollection<Resource> Resources { get; set; }
    }

    [DataContract]
    public class Employee
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string Login { get; set; }

        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public string Surname { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Patronymic { get; set; }

        [DataMember]
        public Post Post { get; set; }

        [DataMember]
        public string Phone { get; set; }

        [DataMember]
        public string Mail { get; set; }

        [DataMember]
        public DateTime Birthday { get; set; }

        [DataMember]
        public Role Role { get; set; }

        [DataMember]
        public ICollection<Contact> Contacts { get; set; }

        [DataMember]
        public ICollection<Meeting> ParticipantMeetings { get; set; }

        [DataMember]
        public ICollection<Meeting> OrganizerMeetings { get; set; }
    }

    [DataContract]
    public class Role
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public ICollection<Employee> Employee { get; set; }
    }

    [DataContract]
    public class Post
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public ICollection<Employee> Employee { get; set; }
    }

    [DataContract]
    public class Meeting
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public ICollection<Employee> Organizers { get; set; }

        [DataMember]
        public ICollection<Employee> Participants { get; set; }

        [DataMember]
        public DateTime StartDate { get; set; }

        [DataMember]
        public DateTime EndDate { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public ICollection<Resource> Resources { get; set; }
    }

    [DataContract]
    public class Contact
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string Surname { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Patronymic { get; set; }

        [DataMember]
        public string Company { get; set; }

        [DataMember]
        public string Post { get; set; }

        [DataMember]
        public string Phone { get; set; }

        [DataMember]
        public string Mail { get; set; }

        [DataMember]
        public DateTime Birthday { get; set; }

        [DataMember]
        public Employee Employee { get; set; }
    }
}
