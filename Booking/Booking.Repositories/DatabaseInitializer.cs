﻿using Booking.Repositories.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Booking.Repositories
{
    class DatabaseInitializer : CreateDatabaseIfNotExists<DataContext>
    {
        protected override void Seed(DataContext context)
        {
            context.TypesOfResources.AddRange(new List<TypeOfResource>
            {
                new TypeOfResource { Name = "Комната" },
                new TypeOfResource { Name = "Аппаратура" }
            });
            context.SaveChanges();

            context.Resources.AddRange(new List<Resource>
            {
                new Resource { Name = "Комната 111", Address = "Юбилейная, 31е", Capacity = 20,
                    TypeOfResource = context.TypesOfResources.First(t => t.Name == "Комната") },
                new Resource { Name = "Комната 222", Address = "Юбилейная, 31е", Capacity = 20,
                    TypeOfResource = context.TypesOfResources.First(t => t.Name == "Комната") },
                new Resource { Name = "Проектор 01", Address = "Юбилейная, 31е", Capacity = 0,
                    TypeOfResource = context.TypesOfResources.First(t => t.Name == "Аппаратура") }
            });
            context.SaveChanges();

            context.Posts.AddRange(new List<Post>
            {
                new Post { Name = ".NET-разработчик" },
                new Post { Name = "Java-разработчик" },
                new Post { Name = "Тестировщик" }
            });
            context.SaveChanges();

            context.Roles.AddRange(new List<Role>
            {
                new Role { Name = "Участник" },
                new Role { Name = "Организатор" }
            });
            context.SaveChanges();

            context.Employees.Add(new Employee
            {
                Login = "test",
                Password = "123",
                Name = "Пользователь",
                Surname = "Тестовый",
                Mail = "asd@gmail.com",
                Post = context.Posts.Where(p => p.Name == ".NET-разработчик").First(),
                Role = context.Roles.Where(p => p.Name == "Организатор").First()
            });
            context.SaveChanges();
        }
    }
}
