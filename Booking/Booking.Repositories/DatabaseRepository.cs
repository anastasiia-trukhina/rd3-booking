﻿using Booking.Repositories.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Booking.Repositories
{
    public class DatabaseRepository : IRepository
    {
        private DataContext _dataContext;

        public DatabaseRepository()
        {
            _dataContext = new DataContext();
        }

        public IEnumerable<Resource> GetResources()
        {
            var AllResources = _dataContext.Resources.Include(x => x.TypeOfResource).ToList();
            AllResources.ForEach(x => x.TypeOfResource.Resources.Clear());
            return AllResources;
        }

        public IEnumerable<Resource> GetResourcesByType(int typeId)
        {
            return (from p in _dataContext.Set<Resource>()
                    where p.TypeOfResource.ID == typeId
                    select new
                    {
                        ID = p.ID,
                        Name = p.Name,
                        Address = p.Address,
                        Capacity = p.Capacity,
                        Meetings = p.Meetings,
                        TypeOfResource = _dataContext.TypesOfResources.FirstOrDefault(x => x.ID == typeId)
                    }).ToList()
                    .Select(x => new Resource
                    {
                        ID = x.ID,
                        Name = x.Name,
                        Address = x.Address,
                        Capacity = x.Capacity,
                        Meetings = x.Meetings,
                        TypeOfResource = _dataContext.TypesOfResources.FirstOrDefault(y => y.ID == x.TypeOfResource.ID)
                    });
        }

        public IEnumerable<Employee> GetEmployees()
        {
            return (from p in _dataContext.Set<Employee>()
                    select new
                    {
                        ID = p.ID,
                        Login = p.Login,
                        Password = p.Password,
                        Surname = p.Surname,
                        Name = p.Name,
                        Patronymic = p.Patronymic,
                        Post = p.Post,
                        Phone = p.Phone,
                        Mail = p.Mail,
                        Birthday = p.Birthday,
                        Role = p.Role,
                        Contacts = p.Contacts,
                        ParticipantMeetings = p.ParticipantMeetings,
                        OrganizerMeetings = p.OrganizerMeetings
                    }).ToList()
                    .Select(x => new Employee
                    {
                        ID = x.ID,
                        Login = x.Login,
                        Password = x.Password,
                        Surname = x.Surname,
                        Name = x.Name,
                        Patronymic = x.Patronymic,
                        Post = x.Post,
                        Phone = x.Phone,
                        Mail = x.Mail,
                        Birthday = x.Birthday,
                        Role = x.Role,
                        Contacts = x.Contacts,
                        ParticipantMeetings = x.ParticipantMeetings,
                        OrganizerMeetings = x.OrganizerMeetings
                    });
        }

        public Employee SignIn(string login, string passwordHash)
        {
            var AllEmployees = _dataContext.Employees.ToList();
            var Employee = AllEmployees.FirstOrDefault(x => x.Login == login && x.Password == passwordHash);
            return Employee;
        }

        public void EmployeeRegistration(Employee employee)
        {
            var role = _dataContext.Roles.First(x => x.Name == "Участник");
            role.Employee.Clear();
            role.Employee.Add(employee);
            employee.Role = role;
            var post = _dataContext.Posts.First(x => x.Name == employee.Post.Name);
            post.Employee.Clear();
            post.Employee.Add(employee);
            employee.Post = post;
            _dataContext.Employees.Add(employee);
            _dataContext.SaveChanges();
        }

        public Employee GetEmployeeByLoginAndPassword(string login, string password)
        {
            return (from p in _dataContext.Set<Employee>()
                    where p.Login == login && p.Password == password
                    select new
                    {
                        ID = p.ID,
                        Login = p.Login,
                        Password = p.Password,
                        Surname = p.Surname,
                        Name = p.Name,
                        Patronymic = p.Patronymic,
                        Post = p.Post,
                        Phone = p.Phone,
                        Mail = p.Mail,
                        Birthday = p.Birthday,
                        Role = p.Role,
                        Contacts = p.Contacts,
                        ParticipantMeetings = p.ParticipantMeetings,
                        OrganizerMeetings = p.OrganizerMeetings
                    }).ToList()
                    .Select(x => new Employee
                    {
                        ID = x.ID,
                        Login = x.Login,
                        Password = x.Password,
                        Surname = x.Surname,
                        Name = x.Name,
                        Patronymic = x.Patronymic,
                        Post = x.Post,
                        Phone = x.Phone,
                        Mail = x.Mail,
                        Birthday = x.Birthday,
                        Role = x.Role,
                        Contacts = x.Contacts,
                        ParticipantMeetings = x.ParticipantMeetings,
                        OrganizerMeetings = x.OrganizerMeetings
                    })
                    .FirstOrDefault();
        }

        public Employee GetEmployeeById(int userId)
        {
            return (from p in _dataContext.Set<Employee>()
                    where p.ID == userId
                    select new
                    {
                        ID = p.ID,
                        Login = p.Login,
                        Password = p.Password,
                        Surname = p.Surname,
                        Name = p.Name,
                        Patronymic = p.Patronymic,
                        Post = p.Post,
                        Phone = p.Phone,
                        Mail = p.Mail,
                        Birthday = p.Birthday,
                        Role = p.Role,
                        Contacts = p.Contacts,
                        ParticipantMeetings = p.ParticipantMeetings,
                        OrganizerMeetings = p.OrganizerMeetings
                    }).ToList()
                    .Select(x => new Employee
                    {
                        ID = x.ID,
                        Login = x.Login,
                        Password = x.Password,
                        Surname = x.Surname,
                        Name = x.Name,
                        Patronymic = x.Patronymic,
                        Post = x.Post,
                        Phone = x.Phone,
                        Mail = x.Mail,
                        Birthday = x.Birthday,
                        Role = x.Role,
                        Contacts = x.Contacts,
                        ParticipantMeetings = x.ParticipantMeetings,
                        OrganizerMeetings = x.OrganizerMeetings
                    })
                    .FirstOrDefault();
        }

        public Employee GetEmployeeByLogin(string login)
        {
            var AllEmployees = _dataContext.Employees.ToList();
            return AllEmployees.FirstOrDefault(x => x.Login == login);
        }

        public Post GetPostByName(string name)
        {
            return (from p in _dataContext.Set<Post>()
                    where p.Name == name
                    select new
                    {
                        ID = p.ID,
                        Name = p.Name,
                        Employee = p.Employee
                    }).ToList()
                    .Select(x => new Post
                    {
                        ID = x.ID,
                        Name = x.Name,
                        Employee = x.Employee
                    })
                    .First();
        }

        public Role GetRoleByName(string name)
        {
            return (from p in _dataContext.Set<Role>()
                    where p.Name == name
                    select new
                    {
                        ID = p.ID,
                        Name = p.Name,
                        Employee = p.Employee
                    }).ToList()
                    .Select(x => new Role
                    {
                        ID = x.ID,
                        Name = x.Name,
                        Employee = x.Employee
                    })
                    .First();
        }

        public IEnumerable<Post> GetAllPosts()
        {
            return (from p in _dataContext.Set<Post>()
                    select new
                    {
                        ID = p.ID,
                        Name = p.Name,
                        Employee = p.Employee
                    }).ToList()
                    .Select(x => new Post
                    {
                        ID = x.ID,
                        Name = x.Name,
                        Employee = x.Employee
                    });
        }

        public IEnumerable<Role> GetAllRoles()
        {
            return (from p in _dataContext.Set<Role>()
                    select new
                    {
                        ID = p.ID,
                        Name = p.Name,
                        Employee = p.Employee
                    }).ToList()
                    .Select(x => new Role
                    {
                        ID = x.ID,
                        Name = x.Name,
                        Employee = x.Employee
                    });
        }

        public Meeting GetMeetingAsParticipant(DateTime date, int userId)
        {
            return (from p in _dataContext.Set<Meeting>()
                    where p.StartDate.Day.ToString() == date.Day.ToString() && p.Participants.Any(x => x.ID == userId)
                    select new
                    {
                        ID = p.ID,
                        Name = p.Name,
                        Organizers = p.Organizers,
                        Participants = p.Participants,
                        StartDate = p.StartDate,
                        EndDate = p.EndDate,
                        Description = p.Description,
                        Resources = p.Resources
                    }).ToList()
                    .Select(x => new Meeting
                    {
                        ID = x.ID,
                        Name = x.Name,
                        Organizers = x.Organizers,
                        Participants = x.Participants,
                        StartDate = x.StartDate,
                        EndDate = x.EndDate,
                        Description = x.Description,
                        Resources = x.Resources
                    })
                    .First();
        }

        public Meeting GetMeetingAsOrganizer(DateTime date, int userId)
        {
            return (from p in _dataContext.Set<Meeting>()
                    where p.StartDate.Day.ToString() == date.Day.ToString() && p.Organizers.Any(x => x.ID == userId)
                    select new
                    {
                        ID = p.ID,
                        Name = p.Name,
                        Organizers = p.Organizers,
                        Participants = p.Participants,
                        StartDate = p.StartDate,
                        EndDate = p.EndDate,
                        Description = p.Description,
                        Resources = p.Resources
                    }).ToList()
                    .Select(x => new Meeting
                    {
                        ID = x.ID,
                        Name = x.Name,
                        Organizers = x.Organizers,
                        Participants = x.Participants,
                        StartDate = x.StartDate,
                        EndDate = x.EndDate,
                        Description = x.Description,
                        Resources = x.Resources
                    })
                    .FirstOrDefault();
        }

        public IEnumerable<Employee> GetOrganizers(int userId)
        {
            return (from p in _dataContext.Set<Employee>()
                    where p.Role.Name == "Организатор" && p.ID != userId
                    select new
                    {
                        ID = p.ID,
                        Login = p.Login,
                        Password = p.Password,
                        Surname = p.Surname,
                        Name = p.Name,
                        Patronymic = p.Patronymic,
                        Post = p.Post,
                        Phone = p.Phone,
                        Mail = p.Mail,
                        Birthday = p.Birthday,
                        Role = p.Role,
                        Contacts = p.Contacts,
                        ParticipantMeetings = p.ParticipantMeetings,
                        OrganizerMeetings = p.OrganizerMeetings
                    }).ToList()
                    .Select(x => new Employee
                    {
                        ID = x.ID,
                        Login = x.Login,
                        Password = x.Password,
                        Surname = x.Surname,
                        Name = x.Name,
                        Patronymic = x.Patronymic,
                        Post = x.Post,
                        Phone = x.Phone,
                        Mail = x.Mail,
                        Birthday = x.Birthday,
                        Role = x.Role,
                        Contacts = x.Contacts,
                        ParticipantMeetings = x.ParticipantMeetings,
                        OrganizerMeetings = x.OrganizerMeetings
                    });
        }

        public IEnumerable<Employee> GetParticipants(int userId)
        {
            return (from p in _dataContext.Set<Employee>()
                    where p.ID != userId
                    select new
                    {
                        ID = p.ID,
                        Login = p.Login,
                        Password = p.Password,
                        Surname = p.Surname,
                        Name = p.Name,
                        Patronymic = p.Patronymic,
                        Post = p.Post,
                        Phone = p.Phone,
                        Mail = p.Mail,
                        Birthday = p.Birthday,
                        Role = p.Role,
                        Contacts = p.Contacts,
                        ParticipantMeetings = p.ParticipantMeetings,
                        OrganizerMeetings = p.OrganizerMeetings
                    }).ToList()
                    .Select(x => new Employee
                    {
                        ID = x.ID,
                        Login = x.Login,
                        Password = x.Password,
                        Surname = x.Surname,
                        Name = x.Name,
                        Patronymic = x.Patronymic,
                        Post = x.Post,
                        Phone = x.Phone,
                        Mail = x.Mail,
                        Birthday = x.Birthday,
                        Role = x.Role,
                        Contacts = x.Contacts,
                        ParticipantMeetings = x.ParticipantMeetings,
                        OrganizerMeetings = x.OrganizerMeetings
                    });
        }

        public void SetMeeting(Meeting meeting)
        {
            var resources = new List<Resource>();
            var AllResources = _dataContext.Set<Resource>().ToList();
            foreach (var i in meeting.Resources)
            {
                foreach (var j in AllResources)
                {
                    if (i.ID == j.ID)
                    {
                        resources.Add(j);
                    }
                }
            }


            var participants = new List<Employee>();

            foreach (var i in meeting.Participants)
            {
                foreach (var j in _dataContext.Employees)
                {
                    if (i.ID == j.ID)
                    {
                        participants.Add(j);
                    }
                }
            }

            var organizers = new List<Employee>();
            foreach (var i in meeting.Organizers)
            {
                foreach (var j in _dataContext.Employees)
                {
                    if (i.ID == j.ID)
                    {
                        organizers.Add(j);
                    }
                }
            }
    
            meeting.Organizers = organizers;
            meeting.Participants = participants;
            meeting.Resources = resources;

            _dataContext.Meetings.Add(meeting);
            _dataContext.SaveChanges();
        }

        public IEnumerable<Meeting> GetMeetingsAsParticipant(int userId)
        {
            return (from p in _dataContext.Set<Meeting>()
                    where p.Organizers.Any(x => x.ID == userId) || p.Participants.Any(x => x.ID == userId)
                    select new
                    {
                        ID = p.ID,
                        Name = p.Name,
                        Organizers = p.Organizers,
                        Participants = p.Participants,
                        StartDate = p.StartDate,
                        EndDate = p.EndDate,
                        Description = p.Description,
                        Resources = p.Resources
                    }).ToList()
                    .Select(x => new Meeting
                    {
                        ID = x.ID,
                        Name = x.Name,
                        Organizers = x.Organizers,
                        Participants = x.Participants,
                        StartDate = x.StartDate,
                        EndDate = x.EndDate,
                        Description = x.Description,
                        Resources = x.Resources
                    });
        }

        public IEnumerable<Meeting> GetMeetingsAsOrganizer(int userId)
        {
            return (from p in _dataContext.Set<Meeting>()
                    where p.Organizers.Any(x => x.ID == userId)
                    select new
                    {
                        ID = p.ID,
                        Name = p.Name,
                        Organizers = p.Organizers,
                        Participants = p.Participants,
                        StartDate = p.StartDate,
                        EndDate = p.EndDate,
                        Description = p.Description,
                        Resources = p.Resources
                    }).ToList()
                    .Select(x => new Meeting
                    {
                        ID = x.ID,
                        Name = x.Name,
                        Organizers = x.Organizers,
                        Participants = x.Participants,
                        StartDate = x.StartDate,
                        EndDate = x.EndDate,
                        Description = x.Description,
                        Resources = x.Resources
                    });
        }

        public void DeleteMeeting(int id)
        {
            if (_dataContext.Meetings.First(x => x.ID == id) != null)
            {
                var RemovingMeeting = _dataContext.Meetings.First(x => x.ID == id);
                _dataContext.Meetings.Remove(RemovingMeeting);
                _dataContext.SaveChanges();
            }

            return;
        }

        public void EditMeeting(Meeting meeting)
        {
            var EditingMeeting = _dataContext.Set<Meeting>().Include(x => x.Resources).Include(x => x.Organizers).Include(x => x.Participants).First(x => x.ID == meeting.ID);
            
            if (meeting.Name != EditingMeeting.Name)
            {
                EditingMeeting.Name = meeting.Name;
            }

            if (meeting.Description != EditingMeeting.Description)
            {
                EditingMeeting.Description = meeting.Description;
            }

            if (meeting.StartDate != EditingMeeting.StartDate)
            {
                EditingMeeting.StartDate = meeting.StartDate;
            }

            if (meeting.EndDate != EditingMeeting.EndDate)
            {
                EditingMeeting.EndDate = meeting.EndDate;
            }

            if (meeting.Organizers.Count == 0)
            {
                EditingMeeting.Organizers.Clear();
            }

            if (meeting.Organizers != EditingMeeting.Organizers)
            {
                EditingMeeting.Organizers.Clear();
                var AllEmployees = _dataContext.Set<Employee>().Include(x => x.Post).Include(x => x.Role).Include(x => x.OrganizerMeetings).Include(x => x.ParticipantMeetings).Include(x => x.Contacts).ToList();

                foreach (var i in meeting.Organizers)
                {
                    EditingMeeting.Organizers.Add(AllEmployees.First(x => x.ID == i.ID));
                }
            }

            if (meeting.Participants.Count == 0)
            {
                EditingMeeting.Participants.Clear();
            }

            if (meeting.Participants != EditingMeeting.Participants)
            {
                EditingMeeting.Participants.Clear();
                var AllEmployees = _dataContext.Set<Employee>().Include(x => x.Post).Include(x => x.Role).Include(x => x.OrganizerMeetings).Include(x => x.ParticipantMeetings).Include(x => x.Contacts).ToList();

                foreach (var i in meeting.Participants)
                {
                    EditingMeeting.Participants.Add(AllEmployees.First(x => x.ID == i.ID));
                }
            }

            if (meeting.Resources.Count == 0)
            {
                EditingMeeting.Resources.Clear();
            }

            if (meeting.Resources != EditingMeeting.Resources)
            {
                EditingMeeting.Resources.Clear();
                var AllResources = _dataContext.Set<Resource>().Include(x => x.Meetings).Include(x => x.TypeOfResource).ToList();

                foreach (var i in meeting.Resources)
                {
                    EditingMeeting.Resources.Add(AllResources.First(x => x.ID == i.ID));
                }
            }

            _dataContext.SaveChanges();
        }

        public IEnumerable<TypeOfResource> GetTypes()
        {
            return _dataContext.Set<TypeOfResource>().ToList();
        }

        public void AddNewResource(Resource resource)
        {
            var typeOfResource = _dataContext.TypesOfResources.First(x => x.Name == resource.TypeOfResource.Name);
            resource.TypeOfResource = typeOfResource;
            _dataContext.Set<Resource>().Add(resource);
            _dataContext.SaveChanges();
        }

        public void AddNewTypeOfResource(TypeOfResource type)
        {
            _dataContext.Set<TypeOfResource>().Add(type);
            _dataContext.SaveChanges();
        }

        public void UpdateResource(Resource resource)
        {
            var EditingResource = _dataContext.Set<Resource>().Include(x => x.TypeOfResource).Include(x => x.Meetings).First(x => x.ID == resource.ID);

            if (resource.Name != EditingResource.Name)
            {
                EditingResource.Name = resource.Name;
            }

            if (resource.Address != EditingResource.Address)
            {
                EditingResource.Address = resource.Address;
            }

            if (resource.Capacity != EditingResource.Capacity)
            {
                EditingResource.Capacity = resource.Capacity;
            }

            if (resource.TypeOfResource.ID != EditingResource.TypeOfResource.ID)
            {
                EditingResource.TypeOfResource = _dataContext.TypesOfResources.FirstOrDefault(x => x.ID == resource.TypeOfResource.ID);
            }

            if (resource.Meetings != EditingResource.Meetings)
            {
                EditingResource.Meetings.Clear();
                var AllMeetings = _dataContext.Set<Meeting>()
                    .Include(x => x.Resources)
                    .Include(x => x.Participants)
                    .Include(x => x.Organizers)
                    .ToList();

                foreach (var i in resource.Meetings)
                {
                    EditingResource.Meetings.Add(AllMeetings.First(x => x.ID == i.ID));
                }
            }

            _dataContext.SaveChanges();
        }

        public void DeleteResource(int id)
        {
            var RemovingResource = _dataContext.Resources.First(x => x.ID == id);
            _dataContext.Resources.Remove(RemovingResource);
            _dataContext.SaveChanges();
        }

        public void UpdateEmployeeAsOrganizer(Employee employee)
        {
            var UpdatingEmployee = _dataContext.Set<Employee>()
                .Include(x => x.Contacts)
                .Include(x => x.OrganizerMeetings)
                .Include(x => x.ParticipantMeetings)
                .Include(x => x.Post)
                .Include(x =>x.Role)
                .First(x => x.ID == employee.ID);

            if (UpdatingEmployee.ParticipantMeetings.Where(x => x.EndDate < DateTime.Now).Count() > 4)
            {
                var NewRole = _dataContext.Roles.First(x => x.Name == "Организатор");
                UpdatingEmployee.Role = NewRole;
                _dataContext.SaveChanges();
            }
        }
    }
}
