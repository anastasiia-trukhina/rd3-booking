﻿using Booking.Repositories.Models;
using System.Data.Entity;

namespace Booking.Repositories
{
    public class DataContext : DbContext
    {
        public DataContext() :
            base("Booking")
        {
            var ensureDLLIsCopied =
                 System.Data.Entity.SqlServer.SqlProviderServices.Instance;

            Database.SetInitializer(new DatabaseInitializer());
        }

        public DbSet<TypeOfResource> TypesOfResources { get; set; }
        public DbSet<Resource> Resources { get; set; }
        public DbSet<Meeting> Meetings { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Contact> Contacts { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new TypeOfResource.TypeOfResourceConfiguration());
            modelBuilder.Configurations.Add(new Resource.ResourceConfiguration());
            modelBuilder.Configurations.Add(new Meeting.MeetingConfiguration());
            modelBuilder.Configurations.Add(new Employee.EmployeeConfiguration());
            modelBuilder.Configurations.Add(new Post.PostConfiguration());
            modelBuilder.Configurations.Add(new Role.RoleConfiguration());
            modelBuilder.Configurations.Add(new Contact.ContactConfiguration());
        }
    }
}
