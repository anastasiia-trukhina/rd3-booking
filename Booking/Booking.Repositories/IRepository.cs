﻿using Booking.Repositories.Models;
using System.Collections.Generic;

namespace Booking.Repositories
{
    public interface IRepository
    {
        IEnumerable<Resource> GetResources();
    }
}
