﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Booking.Repositories.Models
{
    public class Role
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public ICollection<Employee> Employee { get; set; }

        public Role()
        {
            Employee = new HashSet<Employee>();
        }
        public class RoleConfiguration : EntityTypeConfiguration<Role>
        {
            public RoleConfiguration()
            {
                this.ToTable("Roles");
                this.Property(r => r.ID)
                    .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)
                    .IsRequired();
                this.HasKey(r => r.ID);
                this.Property(r => r.Name)
                    .HasMaxLength(100)
                    .IsRequired();
            }
        }
    }
}
