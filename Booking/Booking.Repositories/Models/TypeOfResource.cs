﻿using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace Booking.Repositories.Models
{
    public class TypeOfResource
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public ICollection<Resource> Resources { get; set; }

        public TypeOfResource()
        {
            Resources = new HashSet<Resource>();
        }

        public class TypeOfResourceConfiguration : EntityTypeConfiguration<TypeOfResource>
        {
            public TypeOfResourceConfiguration()
            {
                this.ToTable("TypeOfResources");
                this.Property(t => t.ID)
                    .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)
                    .IsRequired();
                this.HasKey(t => t.ID);
                this.Property(t => t.Name)
                    .HasMaxLength(150)
                    .IsRequired();
            }
        }
    }
}
