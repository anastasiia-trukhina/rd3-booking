﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace Booking.Repositories.Models
{
    public class Employee
    {
        public int ID { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Surname { get; set; }
        public string Name { get; set; }
        public string Patronymic { get; set; }
        public Post Post { get; set; }
        public string Phone { get; set; }
        public string Mail { get; set; }
        public DateTime Birthday { get; set; }
        public Role Role { get; set; }
        public ICollection<Contact> Contacts { get; set; }
        public ICollection<Meeting> ParticipantMeetings { get; set; }
        public ICollection<Meeting> OrganizerMeetings { get; set; }

        public Employee()
        {
            Contacts = new HashSet<Contact>();
            ParticipantMeetings = new HashSet<Meeting>();
            OrganizerMeetings = new HashSet<Meeting>();
        }

        public class EmployeeConfiguration : EntityTypeConfiguration<Employee>
        {
            public EmployeeConfiguration()
            {
                this.ToTable("Employees");
                this.Property(e => e.ID)
                    .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)
                    .IsRequired();
                this.HasKey(e => e.ID);
                this.Property(e => e.Login)
                    .IsRequired();
                this.Property(e => e.Password)
                    .IsRequired();
                this.Property(e => e.Surname)
                    .HasMaxLength(100)
                    .IsRequired();
                this.Property(e => e.Name)
                    .HasMaxLength(100)
                    .IsRequired();
                this.Property(e => e.Patronymic)
                    .HasMaxLength(100);
                this.Property(e => e.Mail)
                    .IsRequired();
                this.Property(e => e.Birthday)
                    .HasColumnType("datetime2");
                this.HasRequired(e => e.Post)
                    .WithMany(p => p.Employee);
                this.HasRequired(e => e.Role)
                    .WithMany(r => r.Employee);
            }
        }

    }
}
