﻿using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace Booking.Repositories.Models
{
    public class Post
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public ICollection<Employee> Employee { get; set; }

        public Post()
        {
            Employee = new HashSet<Employee>();
        }

        public class PostConfiguration : EntityTypeConfiguration<Post>
        {
            public PostConfiguration()
            {
                this.ToTable("Posts");
                this.Property(p => p.ID)
                    .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)
                    .IsRequired();
                this.HasKey(p => p.ID);
                this.Property(p => p.Name)
                    .HasMaxLength(200)
                    .IsRequired();
            }
        }
    }
}
