﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace Booking.Repositories.Models
{
    public class Meeting
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public ICollection<Employee> Organizers { get; set; }
        public ICollection<Employee> Participants { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Description { get; set; }
        public ICollection<Resource> Resources { get; set; }

        public Meeting()
        {
            Resources = new HashSet<Resource>();
            Organizers = new HashSet<Employee>();
            Participants = new HashSet<Employee>();
        }

        public class MeetingConfiguration : EntityTypeConfiguration<Meeting>
        {
            public MeetingConfiguration()
            {
                this.ToTable("Meetings");
                this.Property(m => m.ID)
                    .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)
                    .IsRequired();
                this.HasKey(m => m.ID);
                this.Property(m => m.Name)
                    .HasMaxLength(200)
                    .IsRequired();
                this.Property(m => m.StartDate)
                    .IsRequired();
                this.Property(m => m.EndDate)
                    .IsRequired();
                this.HasMany(m => m.Resources)
                    .WithMany(r => r.Meetings);
                this.HasMany(m => m.Participants)
                    .WithMany(e => e.ParticipantMeetings)
                    .Map(t =>
                    {
                        t.ToTable("MeetingsParticipants");
                        t.MapLeftKey("MeetingID");
                        t.MapRightKey("ParticipantID");
                    });
                this.HasMany(m => m.Organizers)
                    .WithMany(e => e.OrganizerMeetings)
                    .Map(t =>
                    {
                        t.ToTable("MeetingsOrganizers");
                        t.MapLeftKey("MeetingID");
                        t.MapRightKey("OrganizerID");
                    });
            }
        }
    }
}
