﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;

namespace Booking.Repositories.Models
{
    public class Resource
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public TypeOfResource TypeOfResource { get; set; }
        public string Address { get; set; }
        public int Capacity { get; set; }
        public ICollection<Meeting> Meetings { get; set; }

        public Resource()
        {
            Meetings = new HashSet<Meeting>();
        }

        public class ResourceConfiguration : EntityTypeConfiguration<Resource>
        {
            public ResourceConfiguration()
            {
                this.ToTable("Resources");
                this.Property(r => r.ID)
                    .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)
                    .IsRequired();
                this.HasKey(r => r.ID);
                this.Property(r => r.Name)
                    .HasMaxLength(100)
                    .IsRequired();
                this.Property(r => r.Address)
                    .HasMaxLength(150)
                    .IsRequired();
                this.Property(r => r.Capacity)
                    .IsRequired();
                this.HasRequired(r => r.TypeOfResource)
                    .WithMany(t => t.Resources);
                this.HasMany(r => r.Meetings)
                    .WithMany(m => m.Resources);
            }
        }
    }
}
