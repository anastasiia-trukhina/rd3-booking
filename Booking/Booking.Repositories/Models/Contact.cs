﻿using System;
using System.Data.Entity.ModelConfiguration;

namespace Booking.Repositories.Models
{
    public class Contact
    {
        public int ID { get; set; }
        public string Surname { get; set; }
        public string Name { get; set; }
        public string Patronymic { get; set; }
        public string Company { get; set; }
        public string Post { get; set; }
        public string Phone { get; set; }
        public string Mail { get; set; }
        public DateTime Birthday { get; set; }
        public Employee Employee { get; set; }

        public Contact()
        { }

        public class ContactConfiguration : EntityTypeConfiguration<Contact>
        {
            public ContactConfiguration()
            {
                this.ToTable("Contacts");
                this.Property(c => c.ID)
                    .HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity)
                    .IsRequired();
                this.HasKey(c => c.ID);
                this.Property(c => c.Surname)
                    .HasMaxLength(100)
                    .IsRequired();
                this.Property(c => c.Name)
                    .HasMaxLength(100)
                    .IsRequired();
                this.Property(c => c.Patronymic)
                    .HasMaxLength(100);
                this.Property(c => c.Company)
                    .HasMaxLength(100)
                    .IsRequired();
                this.Property(c => c.Post)
                    .HasMaxLength(100)
                    .IsRequired();
                this.Property(c => c.Mail)
                    .IsRequired();
            }
        }
    }
}
