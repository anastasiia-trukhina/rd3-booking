﻿using Booking.Repositories;
using Booking.Repositories.Models;
using System.Collections.Generic;

namespace Booking.BusinessLogic
{
    public class EmployeeManager
    {
        protected readonly DatabaseRepository _databaseRepository;

        public EmployeeManager()
        {
            _databaseRepository = new DatabaseRepository();
        }

        public Employee SignIn(string login, string passwordHash)
        {
            return _databaseRepository.SignIn(login, passwordHash);
        }

        public IEnumerable<Employee> GetAllEmployees()
        {
            return _databaseRepository.GetEmployees();
        }

        public void EmployeeRegistration(Employee employee)
        {
            _databaseRepository.EmployeeRegistration(employee);
        }

        public Employee GetEmployeeByLoginAndPassword(string login, string password)
        {
            return _databaseRepository.GetEmployeeByLoginAndPassword(login, password);
        }

        public Employee GetEmployeeById(int id)
        {
            return _databaseRepository.GetEmployeeById(id);
        }

        public Employee GetEmployeeByLogin(string login)
        {
            return _databaseRepository.GetEmployeeByLogin(login);
        }
        public Post GetPostByName(string name)
        {
            return _databaseRepository.GetPostByName(name);
        }

        public Role GetRoleByName(string name)
        {
            return _databaseRepository.GetRoleByName(name);
        }

        public IEnumerable<Post> GetPosts()
        {
            return _databaseRepository.GetAllPosts();
        }

        public IEnumerable<Role> GetRoles()
        {
            return _databaseRepository.GetAllRoles();
        }

        public IEnumerable<Employee> GetAllOrganizers(int userId)
        {
            return _databaseRepository.GetOrganizers(userId);
        }

        public IEnumerable<Employee> GetAllParticipants(int userId)
        {
            return _databaseRepository.GetParticipants(userId);
        }
    }
}
