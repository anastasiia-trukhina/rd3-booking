﻿using Booking.Repositories;
using Booking.Repositories.Models;
using System.Collections.Generic;

namespace Booking.BusinessLogic
{
    public class ResourceManager
    {
        protected readonly DatabaseRepository _databaseRepository;

        public ResourceManager()
        {
            _databaseRepository = new DatabaseRepository();
        }

        public IEnumerable<Resource> GetAllResources()
        {
            return _databaseRepository.GetResources();
        }

        public IEnumerable<Resource> GetResourcesByType(int typeId)
        {
            return _databaseRepository.GetResourcesByType(typeId);
        }

        public IEnumerable<TypeOfResource> GetAllTypes()
        {
            return _databaseRepository.GetTypes();
        }

        public void AddResource(Resource resource)
        {
            _databaseRepository.AddNewResource(resource);
        }

        public void AddTypeOfResource(TypeOfResource type)
        {
            _databaseRepository.AddNewTypeOfResource(type);
        }

        public void UpdateResource(Resource resource)
        {
            _databaseRepository.UpdateResource(resource);
        }

        public void DeleteResource(int id)
        {
            _databaseRepository.DeleteResource(id);
        }
    }
}
