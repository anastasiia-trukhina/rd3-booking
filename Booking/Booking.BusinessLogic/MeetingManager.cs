﻿using Booking.Repositories;
using Booking.Repositories.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Booking.BusinessLogic
{
    public class MeetingManager
    {
        protected readonly DatabaseRepository _databaseRepository;

        public MeetingManager()
        {
            _databaseRepository = new DatabaseRepository();
        }

        public Meeting GetMeetingAsParticipant(DateTime date, int userId)
        {
            return _databaseRepository.GetMeetingAsParticipant(date, userId);
        }

        public Meeting GetMeetingAsOrganizer(DateTime date, int userId)
        {
            return _databaseRepository.GetMeetingAsOrganizer(date, userId);
        }

        public void SetMeeting(Meeting meeting)
        {
            _databaseRepository.SetMeeting(meeting);
        }

        public IEnumerable<Meeting> GetThreeClosestMeetings(int userId)
        {
            var meetings = _databaseRepository.GetMeetingsAsParticipant(userId);
            var now = DateTime.Now;
            var dicti = new Dictionary<TimeSpan, Meeting>();
            foreach (var i in meetings)
            {
                if (i.StartDate > now)
                {
                    var Distance = (i.StartDate - now).Duration();
                    dicti.Add(Distance, i);
                }
            }

            if (dicti.Count < 3)
            {
                return dicti.Values.ToList();
            }
            else
            {
                var result = new List<Meeting>();
                for (var i = 0; i < 3; i++)
                {
                    result.Add(dicti.OrderBy(x => x.Key).First().Value);
                    dicti.Remove(dicti.OrderBy(x => x.Key).First().Key);
                }

                return result;
            }
        }

        public Meeting GetOrganizersMeeting(int userId)
        {
            var meetings = _databaseRepository.GetMeetingsAsOrganizer(userId);
            var now = DateTime.Now;
            var dicti = new Dictionary<TimeSpan, Meeting>();
            foreach (var i in meetings)
            {
                if (i.StartDate > now)
                {
                    var Distance = (i.StartDate - now).Duration();
                    dicti.Add(Distance, i);
                }
            }

            if (dicti.OrderBy(x => x.Key) != null)
            {
                return dicti.OrderBy(x => x.Key).FirstOrDefault().Value;
            }

            return null;
        }

        public IEnumerable<Meeting> GetMeetingsAsParticipant(int userId)
        {
            return _databaseRepository.GetMeetingsAsParticipant(userId);
        }

        public IEnumerable<Meeting> GetMeetingsAsOrganizer(int userId)
        {
            return _databaseRepository.GetMeetingsAsOrganizer(userId);
        }

        public void DeleteMeeting(int id)
        {
            _databaseRepository.DeleteMeeting(id);
        }

        public void EditMeeting(Meeting meeting)
        {
            _databaseRepository.EditMeeting(meeting);
        }
    }
}
