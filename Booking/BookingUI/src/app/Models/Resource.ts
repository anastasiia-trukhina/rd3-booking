import { TypeOfResource } from './TypeOfResource';

export class Resource {
    ID: number;
    Name: string;
    TypeOfResource: TypeOfResource;
    Address: string;
    Capacity: number;
    Meetings: number[];
}