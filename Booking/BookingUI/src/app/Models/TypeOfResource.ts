import { Resource } from './Resource';

export class TypeOfResource {
    ID: number;
    Name: string;
    Resources: Resource[];
}