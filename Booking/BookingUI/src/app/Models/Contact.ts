import { Employee } from './Employee';

export class Contact {
    public ID: number;
    public Surname: string;
    public Name: string;
    public Patronymic: string;
    public Company: string;
    public Post: string;
    public Phone: string;
    public Mail: string;
    public Birthday: Date;
    public Employee: Employee;
}