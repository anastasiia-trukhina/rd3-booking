import { Post } from './Post';
import { Role } from './Role';
import { Meeting } from './Meeting';
import { Contact } from './Contact';

export class Employee {
    public ID: number;
    public Login: string;
    public Password: string;
    public Surname: string;
    public Name: string;
    public Patronymic: string;
    public Post: Post;
    public Phone: string;
    public Mail: string;
    public Birthday: Date;
    public Role: Role;
    public Contacts: Contact[] = [];
    public OrganizerMeetings: Meeting[] = [];
    public ParticipantMeetings: Meeting[] = [];
}