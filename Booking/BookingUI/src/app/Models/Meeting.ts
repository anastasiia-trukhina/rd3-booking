import { Resource } from './Resource';
import { Employee } from './Employee';

export class Meeting {
    ID: number;
    Name: string;
    StartDate: Date;
    EndDate: Date;
    Description: string;
    Resources: Resource[] = [];
    Organizers: Employee[] = [];
    Participants: Employee[] = [];
}