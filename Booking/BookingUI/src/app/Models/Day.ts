import { Meeting } from './Meeting';

export class Day {
    Day: Date;
    weekDay: number;
    isOtherMonth: boolean = false;
    isBusy: boolean = false;
    isCompleted: boolean = false;
    Meetings: Meeting[];

    constructor(year: number, month: number, day: number) {
        this.Day = new Date(year,month,day);
        if (this.Day.getDay() == 0)
        {
            this.weekDay = 7;
        }
        else
        {
            this.weekDay = this.Day.getDay();
        }
        this.Meetings = [];
    }

    signOtherMonth() {
        this.isOtherMonth = true;
    }

    signBusyDay() {
        this.isBusy = true;
    }

    signCompletedDay() {
        this.isCompleted = true;
    }
}