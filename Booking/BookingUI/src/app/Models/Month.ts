import { Week } from './Week';

export class Month {
    monthName: Date;
    countOfDays: number;
    monthWeeks: Week[] = [];
    countOfWeeks: number;

    constructor(month: number, year: number){
        this.monthName = new Date(year, month, 1);

        var firstDay = new Date(year, month, 1);
        var lastDay = new Date(year, month+1, 0);

        this.countOfDays = (lastDay.valueOf() - firstDay.valueOf())/1000/60/60/24 + 1;

        this.countOfWeeks = Math.ceil( (lastDay.getDate() - (lastDay.getDay() ? lastDay.getDay() : 7))/7 ) + 1;

        for (var i = 0; i<this.countOfWeeks; i++)
        {
            var firstWeekDay = new Date(year, month, i*7+1);
            this.monthWeeks[i] = new Week(month, year, firstWeekDay);
        }
    }
}