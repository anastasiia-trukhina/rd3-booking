import { Day } from './Day';

export class Week {
    weekNumber: number;
    weekDays: Day[] = [];

    constructor(month: number, year:number, firstWeekDay: Date){
        var currentMonth = month;
        var dayOfWeek = new Day(year, month, firstWeekDay.getDate()).weekDay;
        if (firstWeekDay.getDate() < 7)
        {
            month = month-1;
        }

        while (dayOfWeek != 1){
            firstWeekDay.setHours(-24);
            dayOfWeek = new Day(year, month, firstWeekDay.getDate()).weekDay;
        }

        for (var i = 0; i<dayOfWeek; i++){
            this.weekDays[i] = new Day(year, month, firstWeekDay.getDate()-i-1);
            if (this.weekDays[i].Day.getMonth() != currentMonth)
            {
                this.weekDays[i].signOtherMonth();
            }
        }

        for (var i = 0; i < 7; i++){
            this.weekDays[i] = new Day(year, month, firstWeekDay.getDate()+i);
            if (this.weekDays[i].Day.getMonth() != currentMonth)
            {
                this.weekDays[i].signOtherMonth();
            }
        }
    }
}