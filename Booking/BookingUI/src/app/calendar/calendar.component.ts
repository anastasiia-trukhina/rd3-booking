import { Component, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { FormGroup, FormControl } from '@angular/forms';

import { Month } from '../Models/Month';
import { Employee } from '../Models/Employee';
import { UserService } from '../user/user.service';
import { Day } from '../Models/Day';
import { CreateMeetingService } from '../meeting/meeting.service';
import { Meeting } from '../Models/Meeting';
import { CalendarService } from './calendar.service';
import { Resource } from '../Models/Resource';
import { ResourcesService } from '../resources/recources.service';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent {

  CurrentMonth: number = new Date().getMonth();
  CurrentYear: number = new Date().getFullYear();
  Month: Month;
  User: Employee;
  ClosestMeetings: Meeting[] = [];
  OrganizersClosestMeeting: Meeting;
  modalRef: BsModalRef;
  SelectedMeeting: Meeting;
  SelectedMeetingAddress: string;
  Resources: Resource[] = [];
  Organizers: Employee[] = [];
  Participants: Employee[] = [];

  EditingForm = new FormGroup({
    Name: new FormControl(''),
    Description: new FormControl(''),
    Resources: new FormControl(''),
    StartDate: new FormControl(''),
    EndDate: new FormControl(''),
    Organizers: new FormControl(''),
    Participants: new FormControl('')
  });

  constructor(private UserService: UserService,
              private MeetingService: CreateMeetingService,
              private CalendarService: CalendarService, 
              private ResourceService: ResourcesService,
              private Router: Router,
              private modalService: BsModalService) {
                
    this.UserService.UpdateUser().subscribe(() => {
      this.User = this.UserService.GetUser(); 

      this.Month = new Month(this.CurrentMonth, this.CurrentYear);
      console.warn(this.Month);
      this.MeetingService.GetThreeClosestMeetings()
        .subscribe(x => {
          x.forEach(element => {
            this.ClosestMeetings.push(element);
          });
        });

      if (this.User.Role.Name == 'Организатор')
      {
        this.MeetingService.GetOrganizersClosestMeeting()
          .subscribe(x => {
            this.OrganizersClosestMeeting = x;
          });
      }

      console.warn(this.User);

      this.SetMeetingsToMonth();
    });

  }

  ShowMeeting(day: Day) {
    if (this.User.Role.Name == 'Организатор')
    {
      this.CalendarService.GetMeetingAsOrganizer(day)
        .subscribe(x => {
          this.SelectedMeeting = x;
        });
    }
    else 
    {
      this.CalendarService.GetMeetingAsParticipant(day)
        .subscribe(x => {
          this.SelectedMeeting = x;
        });
    }

    if (this.SelectedMeeting.Resources.find(x => x.Name.includes('Комната')) != null)
    {
      this.SelectedMeetingAddress = this.SelectedMeeting.Resources.find(x => x.Name.includes('Комната')).Address;
    }
    else
    {
      this.SelectedMeetingAddress = 'Адрес не указан';
    }
  }

  DeleteMeeting() {
    this.CalendarService.DeleteMeeting(this.SelectedMeeting.ID);
    this.modalRef.hide();
  }

  EditMeeting() {
    this.CalendarService.UpdateMeeting(this.SelectedMeeting);
  }

  onSubmit() {
    let id = this.SelectedMeeting.ID;
    console.warn(this.SelectedMeeting);
    console.warn(this.SelectedMeeting.Resources);
    this.SelectedMeeting = this.EditingForm.getRawValue();
    console.warn(this.EditingForm.getRawValue());
    this.SelectedMeeting.ID = id;
    this.EditMeeting();
    //this.SetMeetingsToMonth();
  }

  // Переход на страницу для организации встречи
  CreateMeeting(day: Day) {
    console.warn(day);
    this.MeetingService.SetDate(day);
    if (this.User.Role.Name=='Организатор') 
    {
      this.Router.navigate(['/create-meeting']);
    }
  }

  GoHome() {
    this.Router.navigate(['/']);
  }

  // Установка полученных с базы встреч в текущий месяц
  SetMeetingsToMonth() {
    this.UserService.UpdateUser();
    this.User = this.UserService.GetUser();
    let Now = new Date();
    this.Month.monthWeeks.forEach(x => {
      x.weekDays.forEach(y => {
        let CurrentDate = new Day(this.CurrentYear, this.CurrentMonth, y.Day.getDate()).Day;

        this.User.ParticipantMeetings.forEach(z => {
          let MeetingDate = new Date(z.StartDate.toString());
          if (CurrentDate.getDate() == MeetingDate.getDate())
          {
            y.Meetings.push(z);
            y.signBusyDay();
            if (Now.getDate() > MeetingDate.getDate())
            {
              y.signCompletedDay();
            }
          }
        })

        this.User.OrganizerMeetings.forEach(k => {
          let MeetingDate = new Date(k.StartDate.toString());
          if (CurrentDate.getDate() == MeetingDate.getDate())
          {
            y.Meetings.push(k);
            y.signBusyDay();
            if (Now.getDate() > MeetingDate.getDate())
            {
              y.signCompletedDay();
            }
          }
        })
      })
    });
  }

  // Открытие модальной страницы для просмотра информации
  openModalInfo(template: TemplateRef<any>, day: Day) {
    this.ShowMeeting(day);
    this.modalRef = this.modalService.show(template);
  }

  openModalEdit(template: TemplateRef<any>) {
    this.UserService.GetOrganizers()
      .subscribe(x => {
        this.Organizers = x;
      });

    this.UserService.GetParticipants()
      .subscribe(x => {
        this.Participants = x;
      });

    this.ResourceService.GetResources()
      .subscribe(x => {
        this.Resources = x;
      });

    this.modalRef = this.modalService.show(template);
  }

  ToUserMenu() {
    this.Router.navigate(['/user']);
  }
}
