import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { AppSettings } from '../AppSettings';
import { Meeting } from '../Models/Meeting';
import { UserService } from '../user/user.service';
import { Day } from '../Models/Day';

@Injectable({ providedIn: 'root' })
export class CalendarService {

    Error: any;
    OrganizerMeetings: Meeting[];
    ParticipantMeetings: Meeting[];

    constructor(private Http: HttpClient,
        private UserService: UserService) { }

    // Получение встречи
    GetMeetingAsParticipant(date: Day) {
        return this.Http.get<Meeting>(AppSettings.URL + 'api/meetings/GetMeetingAsParticipant?date='+date.Day.toISOString()+'&userid=' + this.UserService.GetUser().ID);
    }

    // Получение встречи
    GetMeetingAsOrganizer(date : Day) {
        return this.Http.get<Meeting>(AppSettings.URL + 'api/meetings/GetMeetingAsOrganizer?date='+date.Day.toISOString()+'&userid=' + this.UserService.GetUser().ID);
    }

    DeleteMeeting(id) {
        this.Http.delete(AppSettings.URL+'api/meetings/delete?id='+id)
            .subscribe(x => { alert(x) },
                error => { this.Error = error.error; alert(this.Error); console.warn(this.Error) }
            );
    }

    UpdateMeeting(meeting) {
        this.Http.post(AppSettings.URL+'api/meetings/edit', meeting)
            .subscribe(x => { alert(x) },
                error => { this.Error = error.error; alert(this.Error); console.warn(this.Error) }
            );
    }


}