import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgSelectModule } from '@ng-select/ng-select';
import { TooltipModule, ModalModule } from 'ngx-bootstrap';

import { AppComponent } from './app.component';
import { ResourcesComponent } from './resources/resources.component';
import { HomeComponent } from './home/home.component';
import { CalendarComponent } from './calendar/calendar.component';
import { UserComponent } from './user/user.component';
import { CreateMeetingComponent } from './meeting/meeting.component';
import { RegistrationComponent } from './registration/registration.component';
import { BookingMonthPipe } from './Pipes/Month.datepipe';
import { BookingDayPipe } from './Pipes/Day.datepipe';
import { PostsComponent } from './posts/posts.component';
import { RolesComponent } from './roles/roles.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'resources', component: ResourcesComponent },
  { path: 'calendar', component: CalendarComponent },
  { path: 'user', component: UserComponent },
  { path: 'create-meeting', component: CreateMeetingComponent },
  { path: 'registration', component: RegistrationComponent }
];

@NgModule({
  declarations: 
  [ 
    AppComponent, 
    ResourcesComponent, 
    HomeComponent, 
    CalendarComponent, 
    UserComponent, 
    CreateMeetingComponent, 
    RegistrationComponent, 
    BookingMonthPipe, 
    BookingDayPipe, 
    PostsComponent, 
    RolesComponent
  ],
  imports: 
  [ 
    BrowserModule, 
    HttpClientModule, 
    RouterModule.forRoot(routes), 
    ReactiveFormsModule, 
    NgSelectModule,
    TooltipModule.forRoot(),
    ModalModule.forRoot() 
  ],
  providers: 
  [ ],
  bootstrap:
  [ 
    AppComponent 
  ]
})
export class AppModule { }
