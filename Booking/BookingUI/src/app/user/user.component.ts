import { Component, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';

import { Employee } from '../Models/Employee';
import { UserService } from './user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent {

  CurrentUser: Employee;
  modalRef: BsModalRef;
  Organizers: Employee[];
  Participants: Employee[];

  constructor(private UserService: UserService,
              private modalService: BsModalService,
              private Router: Router) { 
    this.GetOrganisers()
      .subscribe(x => {
        this.Organizers = x;
      });

    this.GetParticipants()
      .subscribe(x => {
        this.Participants = x;
      });

      this.CurrentUser = this.UserService.GetUser();
      console.warn(this.CurrentUser);
  }

  GetOrganisers() {
    return this.UserService.GetOrganizers();
  }

  GetParticipants() {
    return this.UserService.GetParticipants();
  }

  // Открытие модальной страницы для просмотра информации
  openModalInfo(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  ToCalendar() {
    this.Router.navigate(['/calendar']);
  }

  ToContacts() {
    console.warn('ToDo');
  }

  ToResources() {
    this.Router.navigate(['/resources']);
  }
}
