import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

import { AppSettings } from '../AppSettings';
import { Employee } from '../Models/Employee';

@Injectable({
    providedIn: 'root'
  })
  export class UserService {
    
    Token: string;
    CurrentUser: Employee;
    AllOrganizers: Employee[] = [];
    AllParticipants: Employee[] = [];

    constructor(private Http: HttpClient) { }
  
    // Установка/получение токена
    SetToken(string) {
      this.Token = string;
    }

    GetToken() {
      return this.Token;
    }

    // Установка пользователя
    SetUser(user) {
      this.CurrentUser = user;
    }

    // Получение пользователя
    GetUser() : Employee {
      return this.CurrentUser;
    }

    // Обновление пользователя
    UpdateUser() {
      return this.Http.get<Employee>(AppSettings.URL+'api/employee/get?id='+this.GetUser().ID).pipe(map(x => {
          this.CurrentUser = x;
          return x;
        }));
    }

    // Получить всех организаторов
    GetOrganizers() {
      return this.Http.get<Employee[]>(AppSettings.URL+'api/organizers/GetAllOrganizers?userid='+this.GetUser().ID);
    }

    // Получить всех участников
    GetParticipants() {
      return this.Http.get<Employee[]>(AppSettings.URL+'api/participants/GetAllParticipants?userid='+this.GetUser().ID);
    }
  }