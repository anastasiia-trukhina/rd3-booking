import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Employee } from '../Models/Employee';
import { AppSettings } from '../AppSettings';

@Injectable({ providedIn: 'root' })
export class RegistrationService {

    Error: any;

    constructor(private http: HttpClient) { }

    // Регистрация пользователя
    RegisterUser(employee: Employee): void {
        this.http.post(AppSettings.URL + 'api/employee/registration', employee )
            .subscribe(x => { alert(x) },
                error => { this.Error = error.error; alert(this.Error); console.warn(this.Error) }
            );
    }
}