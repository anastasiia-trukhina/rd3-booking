import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

import { RegistrationService } from './registration.service';
import { Employee } from '../Models/Employee';
import { Post } from '../Models/Post';
import { PostsService } from '../posts/posts.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css'],
  providers: [RegistrationService, PostsService]
})
export class RegistrationComponent {

  Employee: Employee = new Employee();
  Posts: Post[] = [];

  RegistrationForm = new FormGroup({
    Login: new FormControl(''),
    Password: new FormControl(''),
    Surname: new FormControl(''),
    Name: new FormControl(''),
    Patronymic: new FormControl(''),
    Post: new FormControl(''),
    Phone: new FormControl(''),
    Mail: new FormControl(''),
    Birthday: new FormControl('')
  });

  constructor(  
    private RegistrationService: RegistrationService,
    private PostsService: PostsService) { 
 
    this.PostsService.GetPosts()
      .subscribe(x => {
        this.Posts = x;
      });
  }

  onSubmit() {
    this.Employee = this.RegistrationForm.getRawValue();
    console.warn(this.Employee);
    this.RegistrationService.RegisterUser(this.Employee);
  }
}
