import { Component } from '@angular/core';

import { Post } from '../Models/Post';
import { PostsService } from './posts.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent {

  Posts: Post[];

  constructor(private PostsService: PostsService) { }

  GetAllPosts() {
    return this.PostsService.GetPosts();
  }

  // Получение должности по названию
  GetPostByName(name: string): Post {
    let result;
    this.Posts.forEach(element => {
        if (element.Name == name) {
            result = element;
        }
    });

    return result;
}
}
