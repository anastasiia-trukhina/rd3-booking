import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Post } from '../Models/Post';
import { AppSettings } from '../AppSettings';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class PostsService {

    Posts: Post[];

    constructor(private Http: HttpClient) { }

    // Получение всех должностей
    GetPosts() {
        return this.Http.get<Post[]>(AppSettings.URL + 'api/posts/get');
    }

    // Получение должности по названию
    GetPostByName(name: string): Post {
        this.GetPosts()
            .subscribe(x => {
                this.Posts = x;
            });
            
        let result;
        this.Posts.forEach(element => {
            if (element.Name == name) {
                result = element;
            }
        });

        return result;
    }
}