import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { AppSettings } from '../AppSettings';
import { Day } from '../Models/Day';
import { Meeting } from '../Models/Meeting';
import { UserService } from '../user/user.service';

@Injectable({ providedIn: 'root' })
export class CreateMeetingService {

    Meeting: Meeting;
    Day: Day;
    Error: any;

    constructor(private Http: HttpClient,
                private UserService: UserService) { }

    // "Установка" дня 
    SetDate(day) {
        this.Day = day;
    }

    // Получение дня
    GetDate() {
        return this.Day;
    }

    // Конвертация даты в строку
    ConvertDayToString(day: Date): string {
        return day.getFullYear()+'-'+("0" + (day.getMonth() + 1)).slice(-2) 
        + '-' + ("0" + day.getDate()).slice(-2) + 'T00:00';
    } 

    // "Установка" встречи
    SetMeeting(_meeting) {
        this.Day.signBusyDay();
        this.Http.post(AppSettings.URL + 'api/meetings/post', _meeting)
            .subscribe(x => { alert(x); console.warn(x) },
                error => { this.Error = error.error; alert(this.Error); console.warn(this.Error) }
            );
    }

    // Получение встречи
    GetMeeting() {
        return this.Meeting;
    }

    // Получение ближайшей встречи
    GetThreeClosestMeetings() {
        return this.Http.get<Meeting[]>(AppSettings.URL + 'api/meetings/GetThreeClosestMeetings?userId=' + this.UserService.GetUser().ID);
    }

    // Получение ближайшей встречи, где пользователь - организатор
    GetOrganizersClosestMeeting() {
        return this.Http.get<Meeting>(AppSettings.URL + 'api/meetings/GetOrganizersClosestMeeting?userId=' + this.UserService.GetUser().ID);
    }
}