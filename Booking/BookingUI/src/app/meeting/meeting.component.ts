import { Component } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

import { Resource } from '../Models/Resource';
import { ResourcesService } from '../resources/recources.service';
import { Meeting } from '../Models/Meeting';
import { UserService } from '../user/user.service';
import { Employee } from '../Models/Employee';
import { CreateMeetingService } from './meeting.service';

@Component({
  selector: 'app-meeting',
  templateUrl: './meeting.component.html',
  styleUrls: ['./meeting.component.css']
})
export class CreateMeetingComponent {
  
  Resources: Resource[];
  Meeting: Meeting;
  StartDate: string;
  EndDate: string;
  CurrentUser: Employee[] = [];
  AllOrganizers: Employee[] = [];
  AllParticipants: Employee[] = [];

  MeetingForm = new FormGroup({
    Name: new FormControl(''),
    Description: new FormControl(''),
    Resources: new FormControl(''),
    StartDate: new FormControl(''),
    EndDate: new FormControl(''),
    Organizers: new FormControl(''),
    Participants: new FormControl('')
  });

  constructor(private ResourceService: ResourcesService,
              private MeetingService: CreateMeetingService,
              private UserService: UserService,
              private Router: Router) {

    this.ResourceService.GetResources()
      .subscribe(x => {
        this.Resources = x;
      });

    this.UserService.GetOrganizers()
      .subscribe(x => {
        this.AllOrganizers=x;
      });

    this.UserService.GetParticipants()
      .subscribe(x => {
        this.AllParticipants=x;
      });

    this.CurrentUser.push(this.UserService.GetUser());

    if (this.CurrentUser[0].Role.Name=='Организатор')
    {
      this.AllOrganizers.push(this.CurrentUser[0]);
    }
    else
    {
      this.AllParticipants.push(this.CurrentUser[0]);
    }

    this.StartDate = this.MeetingService.ConvertDayToString(this.MeetingService.GetDate().Day);
    this.EndDate = this.StartDate;
  }

  // Организация встречи
  onSubmit() {
    this.Meeting = this.MeetingForm.getRawValue();
    console.warn(this.Meeting);
    this.MeetingService.SetMeeting(this.Meeting);
    this.UserService.UpdateUser();
  }

  // Переход к календарю
  ToCalendar() {
    this.Router.navigate(['/calendar']);
  }
}
