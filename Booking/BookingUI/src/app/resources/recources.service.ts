import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Resource } from '../Models/Resource';
import { AppSettings } from '../AppSettings';
import { TypeOfResource } from '../Models/TypeOfResource';

@Injectable({ providedIn: 'root' })
export class ResourcesService {

  Resources: Resource[];
  Error: any;

  constructor(private Http: HttpClient) { 
    this.GetResources()
      .subscribe(x => {
        this.Resources = x;
      });
  }

  // Получение всех ресурсов
  GetResources() : Observable<Resource[]> {
    return this.Http.get<Resource[]>(AppSettings.URL + 'api/resources/get').pipe(
      map(data => this.Resources = data));
  }

  GetTypesOfResources() {
    return this.Http.get<TypeOfResource[]>(AppSettings.URL + 'api/resources/gettypes');
  }

  GetAllResources() {
    return this.Resources;
  }

  AddNewTypeOfResource(type) {
    this.Http.post(AppSettings.URL+'api/resources/AddTypeOfResource', type)
      .subscribe(x => { alert(x) },
        error => { this.Error = error.error; alert(this.Error); console.warn(this.Error) }
      );
  }

  AddNewResource(resource) {
    this.Http.post(AppSettings.URL+'api/resources/AddResource', resource)
      .subscribe(x => { alert(x) },
        error => { this.Error = error.error; alert(this.Error); console.warn(this.Error) }
      );
  }

  UpdateResource(resource) {
    this.Http.post(AppSettings.URL + 'api/resources/updateResource', resource)
      .subscribe(x => { alert(x) },
        error => { this.Error = error.error; alert(this.Error); console.warn(this.Error) }
      );
  }

  DeleteResource(id) {
    this.Http.delete(AppSettings.URL + 'api/resources/deleteResource?id='+id)
      .subscribe(x => { alert(x) },
        error => { this.Error = error.error; alert(this.Error); console.warn(this.Error) }
      );
  }
}