import { Component, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';

import { Resource } from '../Models/Resource';
import { ResourcesService } from './recources.service';
import { FormGroup, FormControl } from '@angular/forms';
import { TypeOfResource } from '../Models/TypeOfResource';
import { CalendarService } from '../calendar/calendar.service';
import { Month } from '../Models/Month';

@Component({
  selector: 'app-resources',
  templateUrl: './resources.component.html',
  styleUrls: ['./resources.component.css']
})
export class ResourcesComponent {
 
  Resources: Resource[];
  modalRef: BsModalRef;
  AllTypesOfResources: TypeOfResource[];
  NewResource: Resource;
  NewTypeOfResource: TypeOfResource;
  Month: Month;
  SelectedResource: Resource;

  AddResourceForm = new FormGroup({
    Name: new FormControl(''),
    TypeOfResource: new FormControl(''),
    Address: new FormControl(''),
    Capacity: new FormControl('')
  });

  AddTypeOfResourceForm = new FormGroup({
    Name: new FormControl('')
  });

  constructor(private ResourceService: ResourcesService,
              private Router: Router,
              private modalService: BsModalService) { 
    this.ResourceService.GetResources()
      .subscribe(x => {
        this.Resources = x;
      });

    this.ResourceService.GetTypesOfResources()
      .subscribe(x => {
        this.AllTypesOfResources = x;
      })

    
  }

  // GetAllResources() {
  //   this.ResourceService.GetResources()
  //     .subscribe(x => {
  //       this.Resources = x;
  //     });
  // }

  UpdateResource(resource) {
    this.ResourceService.UpdateResource(resource);
  }

  DeleteResource(id) {
    this.ResourceService.DeleteResource(id);
  }

  ToUserMenu() {
    this.Router.navigate(['/user']);
  }

  onSubmit() {
    this.NewResource = this.AddResourceForm.getRawValue();
    this.ResourceService.AddNewResource(this.NewResource);
  }

  onUpdateSubmit(id) {
    this.SelectedResource = this.AddResourceForm.getRawValue();
    this.SelectedResource.ID = id;
    this.ResourceService.UpdateResource(this.SelectedResource);
    console.warn(this.SelectedResource);
    this.ResourceService.GetResources()
      .subscribe(x => {
        this.Resources = x;
      });
    console.warn(this.Resources);
  }

  onTypeSubmit() {
    this.NewTypeOfResource = this.AddTypeOfResourceForm.getRawValue();
    this.ResourceService.AddNewTypeOfResource(this.NewTypeOfResource);
  }

  openModalInfo(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }
}
