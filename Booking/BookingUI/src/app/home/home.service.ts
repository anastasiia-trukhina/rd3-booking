import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Employee } from '../Models/Employee';
import { AppSettings } from '../AppSettings';
import { UserService } from '../user/user.service';
import { Auth } from '../Models/Auth';

@Injectable({ providedIn: 'root' })
export class HomeService {

    Error: any;

    constructor(private http: HttpClient,   
                private userService: UserService) { }

    // Получение токена 
    GetToken(Authentication) : Observable<any> {
        let body = 'username='+Authentication.Login+'&password='+Authentication.Password+'&grant_type=password';
        return this.http.post(AppSettings.URL + 'token', body, { headers: new HttpHeaders({ 'Content-type': 'application/x-www-form-urlencoded' }) } );
    }

    GetAccount(Authentication) : Observable<Employee> {
        let headers = new HttpHeaders({
            'Authorization': 'Bearer ' + this.userService.GetToken()
        });

        return this.http.get<Employee>(AppSettings.URL + 'api/account/get?login='+Authentication.Login+'&password='+Authentication.Password, { headers : headers });
    }

    // Прохождение авторизации
    SignIn(Authentication: Auth) {

        this.GetToken(Authentication)
            .subscribe(
                data => { this.userService.SetToken(data.valueOf()['access_token']) },
                error => { this.Error = error.error }
            );    

        console.warn(this.userService.GetToken());

        this.GetAccount(Authentication)
            .subscribe(
                data => { this.userService.SetUser(data) },
                error => { this.Error = error.error }
            );
        
    }
}