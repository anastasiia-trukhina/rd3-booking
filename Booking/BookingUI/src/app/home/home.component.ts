import { Component } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

import { HomeService } from './home.service';
import { Auth } from '../Models/Auth';
import { UserService } from '../user/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent {

  Authentication: Auth;
  isUserNotExist: boolean;
  Error: any;

  AuthenForm = new FormGroup({
    Login: new FormControl(''),
    Password: new FormControl('')
  });

  constructor(private HomeService: HomeService,
              private UserService: UserService,
              private Router: Router) { }

  onSubmit() {
    this.Authentication = this.AuthenForm.getRawValue();
    console.warn(this.Authentication);
    this.HomeService.SignIn(this.Authentication);

    if (this.UserService.GetToken() != null)
    {
      this.isUserNotExist = false;
      console.warn(this.UserService.GetToken());
      console.warn('false');
      this.Router.navigate(['/calendar']);
    }
    else
    {
      this.isUserNotExist = true;
      this.Error = this.HomeService.Error;
      console.warn('true');
    }
  }
  
}
