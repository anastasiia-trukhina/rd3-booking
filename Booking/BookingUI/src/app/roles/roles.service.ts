import { Injectable } from '@angular/core';

import { Role } from '../Models/Role';

@Injectable({ providedIn: 'root' })
export class RolesService {
  
    private Roles: Role[];

    constructor() { }

    SetRoles(x) {
        this.Roles = x;
    }

    // Получение всех ролей
    GetRoles(): Role[] {
        return this.Roles;
    }

    // Получение роли по названию
    GetRoleByName(name: string): Role {
        var result;
        this.Roles.forEach(element => {
            if (element.Name == name) {
                result = element;
            }
        });

        return result;
    }
}