import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { RolesService } from './roles.service';
import { Role } from '../Models/Role';
import { AppSettings } from '../AppSettings';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.css']
})
export class RolesComponent {

  constructor(private Http: HttpClient,
              private RolesService: RolesService) { }

  GetAllRoles() {
    this.Http.get<Role[]>(AppSettings.URL + 'api/roles/get')
      .subscribe(x => { 
          this.RolesService.SetRoles(x); 
      });
  }

}
