﻿using Newtonsoft.Json;
using System;
using System.Text.RegularExpressions;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Booking.WebAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("")]
    public class MeetingsController : ApiController
    {
        protected readonly BookingServiceReference.BookingServiceClient _client = new BookingServiceReference.BookingServiceClient();

        // GET: api/Meetings/GetMeetingAsParticipant
        public IHttpActionResult GetMeetingAsParticipant(DateTime date, int userId)
        {
            return Json(_client.GetMeetingAsParticipant(date, userId));
        }

        // GET: api/Meetings/GetMeetingAsOrganiser
        public IHttpActionResult GetMeetingAsOrganizer(DateTime date, int userId)
        {
            return Json(_client.GetMeetingAsOrganizer(date, userId));
        }

        // GET: api/Meetings/GetClosestMeeting?userId=5
        public IHttpActionResult GetThreeClosestMeetings(int userId)
        {
            return Json(_client.GetThreeClosestMeetings(userId));
        }

        // GET: api/Meetings/GetOrganizersClosestMeeting?userId=5
        public IHttpActionResult GetOrganizersClosestMeeting(int userId)
        {
            return Json(_client.GetOrganizersClosestMeeting(userId));
        }

        // GET: api/Meetings/GetMeetingAsParticipant
        public IHttpActionResult GetMeetingsAsParticipant(int userId)
        {
            return Json(_client.GetMeetingsAsParticipant(userId));
        }

        // GET: api/Meetings/GetMeetingAsOrganiser
        public IHttpActionResult GetMeetingsAsOrganizer(int userId)
        {
            return Json(_client.GetMeetingsAsOrganizer(userId));
        }

        // POST: api/Meetings/Post
        [HttpPost]
        public IHttpActionResult Post()
        {
            var BodyValue = Request.Content.ReadAsStringAsync().GetAwaiter().GetResult();
            var Meeting = new Booking.WCF.Meeting();
            try
            {
                Meeting = JsonConvert.DeserializeObject<Booking.WCF.Meeting>(BodyValue);
            }
            catch (JsonSerializationException ex)
            {
                return Content(System.Net.HttpStatusCode.BadRequest, "Не указана дата рождения");
            }

            var NameRegex = new Regex(@"^[а-яёЁА-Я ]+$");
            if (!NameRegex.IsMatch(Meeting.Name))
            {
                return Content(System.Net.HttpStatusCode.BadRequest, "Название не соответствует условиям");
            }

            var DescriptionRegex = new Regex(@"^[а-яёЁА-Я0-9-_\. ]{1,100}$");
            if (!DescriptionRegex.IsMatch(Meeting.Description))
            {
                return Content(System.Net.HttpStatusCode.BadRequest, "Неопознанные знаки в описании");
            }

            if (Meeting.StartDate > Meeting.EndDate)
            {
                return Content(System.Net.HttpStatusCode.BadRequest, "Дата начала позже даты завершения");
            }

            if (Meeting.Resources == null || Meeting.Resources.Count == 0)
            {
                return Content(System.Net.HttpStatusCode.BadRequest, "Не указаны ресурсы для встречи");
            }

            if (Meeting.Organizers == null || Meeting.Organizers.Count == 0)
            {
                return Content(System.Net.HttpStatusCode.BadRequest, "Должен быть хотя бы один организатор");
            }

            if (Meeting.Participants == null || Meeting.Participants.Count == 0)
            {
                return Content(System.Net.HttpStatusCode.BadRequest, "Отсутствуют участники встречи");
            }

            _client.SetMeeting(Meeting);
            return Ok("Встреча добавлена");
        }

        // POST: api/Meetings/Edit
        [HttpPost]
        public IHttpActionResult Edit()
        {
            var BodyValue = Request.Content.ReadAsStringAsync().GetAwaiter().GetResult();
            var Meeting = new Booking.WCF.Meeting();
            try
            {
                Meeting = JsonConvert.DeserializeObject<Booking.WCF.Meeting>(BodyValue);
            }
            catch (JsonSerializationException ex)
            {
                return Content(System.Net.HttpStatusCode.BadRequest, "Не указана дата");
            }

            var NameRegex = new Regex(@"^[а-яёЁА-Я ]+$");
            if (!NameRegex.IsMatch(Meeting.Name))
            {
                return Content(System.Net.HttpStatusCode.BadRequest, "Название не соответствует условиям");
            }

            var DescriptionRegex = new Regex(@"[а-яёЁА-Я]?${1,100}$");
            if (!DescriptionRegex.IsMatch(Meeting.Description))
            {
                return Content(System.Net.HttpStatusCode.BadRequest, "Неопознанные знаки в описании");
            }

            if (Meeting.StartDate > Meeting.EndDate)
            {
                return Content(System.Net.HttpStatusCode.BadRequest, "Дата начала позже даты завершения");
            }

            if (Meeting.Resources.Count == 0)
            {
                return Content(System.Net.HttpStatusCode.BadRequest, "Не указаны ресурсы для встречи");
            }

            if (Meeting.Organizers.Count == 0)
            {
                return Content(System.Net.HttpStatusCode.BadRequest, "Должен быть хотя бы один организатор");
            }

            if (Meeting.Participants.Count == 0)
            {
                return Content(System.Net.HttpStatusCode.BadRequest, "Отсутствуют участиники встречи");
            }

            _client.EditMeeting(Meeting);
            return Ok("Встреча обновлена");
        }

        // DELETE: api/Meetings/5
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            _client.DeleteMeeting(id);
            return Ok("Встреча удалена");
        }
    }
}
