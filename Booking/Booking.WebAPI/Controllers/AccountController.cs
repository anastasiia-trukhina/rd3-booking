﻿using System.Web.Http;
using System.Web.Http.Cors;

namespace Booking.WebAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("")]
    public class AccountController : ApiController
    {
        protected readonly BookingServiceReference.BookingServiceClient _client = new BookingServiceReference.BookingServiceClient();

        [AllowAnonymous]
        // GET: api/Account
        public IHttpActionResult Get(string login, string password)
        {
            var Employee = _client.SignIn(login, password);
            if (Employee != null)
            {
                return Json(Employee);
            }
            else
            {
                return Content(System.Net.HttpStatusCode.BadRequest, "Пользователя не существует");
            }
        }

        [Authorize]
        public IHttpActionResult GetEmployee(string login, string password)
        {
            return Ok(_client.GetEmployeeByLoginAndPassword(login, password));
        }        
    }
}
