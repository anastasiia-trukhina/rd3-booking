﻿using System.Web.Http;
using System.Web.Http.Cors;

namespace Booking.WebAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("")]
    public class ParticipantsController : ApiController
    {
        protected readonly BookingServiceReference.BookingServiceClient _client = new BookingServiceReference.BookingServiceClient();
        // GET: api/Participants/5
        public IHttpActionResult GetAllParticipants(int userId)
        {
            return Json(_client.GetAllParticipants(userId));
        }
    }
}
