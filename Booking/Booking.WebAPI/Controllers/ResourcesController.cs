﻿using Booking.WCF;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Booking.WebAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("")]
    public class ResourcesController : ApiController
    {
        protected readonly BookingServiceReference.BookingServiceClient _client = new BookingServiceReference.BookingServiceClient();
        // GET: api/Resources
        public IHttpActionResult Get()
        {
            return Json(_client.GetAllResources());
        }

        // GET: api/Resources
        public IHttpActionResult GetTypes()
        {
            return Json(_client.GetTypes());
        }

        [HttpPost]
        public IHttpActionResult AddResource()
        {
            var BodyValue = Request.Content.ReadAsStringAsync().GetAwaiter().GetResult();
            var Resource = new Booking.WCF.Resource();
            try
            {
                Resource = JsonConvert.DeserializeObject<Booking.WCF.Resource>(BodyValue);
            }
            catch (JsonSerializationException ex)
            {
                return Content(System.Net.HttpStatusCode.BadRequest, "Не все поля заполнены");
            }

            var NameRegex = new Regex(@"^[а-яёЁА-Я0-9 ]+$");
            if (!NameRegex.IsMatch(Resource.Name))
            {
                return Content(System.Net.HttpStatusCode.BadRequest, "Название не соответствует условиям");
            }

            if (Resource.TypeOfResource == null)
            {
                return Content(System.Net.HttpStatusCode.BadRequest, "Не выбран тип ресурса");
            }

            if (!NameRegex.IsMatch(Resource.Address))
            {
                return Content(System.Net.HttpStatusCode.BadRequest, "Некорректный адрес");
            }

            if (Resource.TypeOfResource.Name == "Комната" && Resource.Capacity < 3)
            {
                return Content(System.Net.HttpStatusCode.BadRequest, "Это кладовка? Вместимость должна быть больше с:");
            }

            _client.AddResource(Resource);
            return Ok("Ресурс добавлен");
        }

        [HttpPost]
        public IHttpActionResult UpdateResource()
        {
            var BodyValue = Request.Content.ReadAsStringAsync().GetAwaiter().GetResult();
            var Resource = new Booking.WCF.Resource();
            try
            {
                Resource = JsonConvert.DeserializeObject<Booking.WCF.Resource>(BodyValue);
            }
            catch (JsonSerializationException ex)
            {
                return Content(System.Net.HttpStatusCode.BadRequest, "Не указано имя типа");
            }

            var NameRegex = new Regex(@"^[а-яёЁА-Я0-9 ]+$");
            if (!NameRegex.IsMatch(Resource.Name))
            {
                return Content(System.Net.HttpStatusCode.BadRequest, "Название не соответствует условиям");
            }

            if (Resource.TypeOfResource == null)
            {
                return Content(System.Net.HttpStatusCode.BadRequest, "Не выбран тип ресурса");
            }

            /*if (Resource.Address)
            {
                return Content(System.Net.HttpStatusCode.BadRequest, "Некорректный адрес");
            }*/

            if (Resource.TypeOfResource.Name == "Комната" && Resource.Capacity < 3)
            {
                return Content(System.Net.HttpStatusCode.BadRequest, "Это кладовка? Вместимость должна быть больше с:");
            }

            _client.UpdateResource(Resource);
            return Ok("Ресурс обновлен");
        }

        [HttpDelete]
        public void DeleteResource(int id)
        {
            _client.DeleteResource(id);
        }

        [HttpPost]
        public IHttpActionResult AddTypeOfResource()
        {
            var BodyValue = Request.Content.ReadAsStringAsync().GetAwaiter().GetResult();
            var TypeOfResource = new Booking.WCF.TypeOfResource();
            try
            {
                TypeOfResource = JsonConvert.DeserializeObject<Booking.WCF.TypeOfResource>(BodyValue);
            }
            catch (JsonSerializationException ex)
            {
                return Content(System.Net.HttpStatusCode.BadRequest, "Не указано имя типа");
            }

            if (TypeOfResource.Name == "")
            {
                return Content(System.Net.HttpStatusCode.BadRequest, "Не указано имя типа");
            }

            _client.AddTypeOfResource(TypeOfResource);
            return Ok("Тип ресурса добавлен");
        }

        // GET: api/Resources/get?typeId=5
        public IEnumerable<Resource> Get(int typeId)
        {
            return _client.GetResourcesByType(typeId);
        }
    }
}
