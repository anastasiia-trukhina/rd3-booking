﻿using Newtonsoft.Json;
using System;
using System.Text.RegularExpressions;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Booking.WebAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("")]
    public class EmployeeController : ApiController
    {
        protected readonly BookingServiceReference.BookingServiceClient _client = new BookingServiceReference.BookingServiceClient();
        // GET: api/Employee
        public IHttpActionResult Get()
        {
            return Json(_client.GetAllEmployees());
        }

        // GET: api/Employee/5
        public IHttpActionResult Get(int id)
        {
            return Json(_client.GetEmployeeById(id));
        }

        // POST: api/Employee
        [HttpPost]
        public IHttpActionResult Registration()
        {
            var BodyValue = Request.Content.ReadAsStringAsync().GetAwaiter().GetResult();
            var Employee = new Booking.WCF.Employee();
            try
            {
                Employee = JsonConvert.DeserializeObject<Booking.WCF.Employee>(BodyValue);
            }
            catch (JsonSerializationException ex)
            {
                return Content(System.Net.HttpStatusCode.BadRequest, "Не указана дата рождения");
            }

            var LoginRegex = new Regex(@"^[a-zA-Z][a-zA-Z0-9-_\.]{1,20}$");
            if (!LoginRegex.IsMatch(Employee.Login))
            {
                return Content(System.Net.HttpStatusCode.BadRequest, "Логин не соответствует условиям");
            }

            if (_client.GetEmployeeByLogin(Employee.Login) != null)
            {
                return Content(System.Net.HttpStatusCode.BadRequest, "Логин занят");
            }

            if (Employee.Password.Length < 6)
            {
                return Content(System.Net.HttpStatusCode.BadRequest, "Пароль не соответствует условиям");
            }

            var PersonalRegex = new Regex(@"^[\p{L}]+$");
            if (!PersonalRegex.IsMatch(Employee.Name))
            {
                return Content(System.Net.HttpStatusCode.BadRequest, "В поле 'Имя' запрещённые символы");
            }

            if (!PersonalRegex.IsMatch(Employee.Surname))
            {
                return Content(System.Net.HttpStatusCode.BadRequest, "В поле 'Фамилия' запрещённые символы");
            }

            if (!PersonalRegex.IsMatch(Employee.Patronymic))
            {
                return Content(System.Net.HttpStatusCode.BadRequest, "В поле 'Отчество' запрещённые символы");
            }

            var PhoneRegex = new Regex(@"^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$");
            if (!PhoneRegex.IsMatch(Employee.Phone))
            {
                return Content(System.Net.HttpStatusCode.BadRequest, "Неверно введён номер");
            }

            var MailRegex = new Regex(@"^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$");
            if (!MailRegex.IsMatch(Employee.Mail))
            {
                return Content(System.Net.HttpStatusCode.BadRequest, "Неверно введён адрес электронной почты");
            }

            if (Math.Round(DateTime.Now.Subtract(Employee.Birthday).TotalDays/365.25, 2) < 18)
            {
                return Content(System.Net.HttpStatusCode.BadRequest, "Пользователь должен быть старше 18-ти лет");
            }

            _client.EmployeeRegistation(Employee);
            return Ok("Регистрация прошла успешно");
        }
    }
}
