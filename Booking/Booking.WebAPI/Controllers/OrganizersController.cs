﻿using System.Web.Http;
using System.Web.Http.Cors;

namespace Booking.WebAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("")]
    public class OrganizersController : ApiController
    {
        protected readonly BookingServiceReference.BookingServiceClient _client = new BookingServiceReference.BookingServiceClient();

        // GET: api/Organizers
        public IHttpActionResult GetAllOrganizers(int userId)
        {
            return Json(_client.GetAllOrganizers(userId));
        }
    }
}
