﻿using System.Web.Http;
using System.Web.Http.Cors;

namespace Booking.WebAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("")]
    public class PostsController : ApiController
    {
        protected readonly BookingServiceReference.BookingServiceClient _client = new BookingServiceReference.BookingServiceClient();
        // GET: api/Posts
        public IHttpActionResult Get()
        {
            return Json(_client.GetPosts());
        }
    }
}
