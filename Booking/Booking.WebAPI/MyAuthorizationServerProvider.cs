﻿using Microsoft.Owin.Security.OAuth;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Booking.WebAPI
{
    public class MyAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        protected readonly BookingServiceReference.BookingServiceClient _client = new BookingServiceReference.BookingServiceClient();

        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

            var user = _client.GetEmployeeByLoginAndPassword(context.UserName, context.Password);
            var identity = new ClaimsIdentity(context.Options.AuthenticationType);
            
            if (user != null)
            {
                identity.AddClaim(new Claim(ClaimTypes.Role, user.Role.Name));
                identity.AddClaim(new Claim("username", user.Login));
                identity.AddClaim(new Claim(ClaimTypes.Name, user.Name));
                context.Validated(identity);
            }
            else
            {
                context.SetError("invalid_grant", "Provided username and password is incorrect");
                return;
            }
        }
    }
}